from selenium import webdriver
from selenium.webdriver.common.by import By
import time

class Test1:
    def __int__(self, address: str):
        self.driver = webdriver.Chrome(executable_path="C:\\chromedriver.exe") # 드라이버
        self.driver.get(address) # 주소 입력
        self.driver.maximize_window() # 전체 화면

    # 로그인
    def LogIn(self) :
        self.driver.find_element(By.ID, "mui-1").send_keys("ldk_admin")  # 아이디 입력
        self.driver.find_element(By.ID, "mui-2").send_keys("ldk_admin@tutor")  # 비밀번호 입력
        self.driver.find_element(By.CLASS_NAME, "MuiButton-root").click()  # 로그인 버튼 클릭

    # 센터 리스트 > 잠실
    def center_list_check(self):
        self.driver.find_element(By.CLASS_NAME, "MuiSelect-select").click() # 센터 리스트
        self.driver.find_element(By.XPATH, "//li[@data-value='37']").click() # 잠실 센터

    # 로그아웃
    def LogOut(self):
        self.driver.find_element(By.XPATH, "//button[@tabindex='0']").click() # 로그아웃 버튼

    # 로그아웃 > 팝업창 > 승인
    def LogOut_Alert_Accept(self):
        from selenium.webdriver.common.alert import Alert
        da = Alert(self.driver)
        da.accept()

    def do(self):
        self.__int__("http://49.247.24.46:7000/")
        time.sleep(1)
        self.LogIn()
        time.sleep(1)
        self.center_list_check()
        time.sleep(1)
        self.LogOut()
        time.sleep(1)
        self.LogOut_Alert_Accept()

if __name__ == '__main__':
    a = Test1()
    a.do()
    print("clear")
