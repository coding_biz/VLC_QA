from selenium import webdriver
from selenium.webdriver.common.by import By
import time

from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

from selenium.webdriver.chrome.options import Options
import subprocess

import pyautogui

subprocess.Popen(r'C:\Program Files\Google\Chrome\Application\chrome.exe --remote-debugging-port=9222 --user-data-dir="C:\chrometemp"') # 디버거 크롬 구동


class Test3:
    def __init__(self, address: str):
        option = Options()
        option.add_experimental_option("debuggerAddress", "127.0.0.1:9222")
        self.driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=option)
        self.driver.get(address)
        self.driver.maximize_window()

    # 로그인
    def LogIn(self):
        self.driver.find_element(By.XPATH, "//*[@id='root']/div/div/div/div/div/div/div/div[2]/div/div/div/div/div[2]/span[1]").click()
        time.sleep(2)
        main = self.driver.window_handles
        for handle in main:
            if handle == main[1]:
                self.driver.switch_to.window(handle)
        # 아이디 선택에 마우스 오버시켜야 함
        pyautogui.click()

        # time.sleep(3)
        # self.driver.find_element(By.XPATH, "//*[@id='identifierId']").send_keys("dkstudiocs")
        # time.sleep(3)
        # self.driver.find_element(By.XPATH, "//*[@id='identifierNext']/div/button/span").click()

    # 코스등록
    def regist(self):
        self.driver.find_element(By.CLASS_NAME, "MuiSelect-select").click() # 센터 리스트
        self.driver.find_element(By.XPATH, "//li[@data-value='37']").click() # 잠실 센터

    # 로그아웃
    def LogOut(self):
        self.driver.find_element(By.XPATH, "//button[@tabindex='0']").click() # 로그아웃 버튼

    # 로그아웃 > 팝업창 > 승인
    def LogOut_Alert_Accept(self):
        from selenium.webdriver.common.alert import Alert
        da = Alert(self.driver)
        da.accept()

    def do(self):
        self.__init__("https://blue-smoke-0acf0e000.1.azurestaticapps.net/")
        self.LogIn()


if __name__ == '__main__':
    a = Test3("https://blue-smoke-0acf0e000.1.azurestaticapps.net/")
    a.do()
    print("clear")
