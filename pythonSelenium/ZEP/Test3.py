from selenium import webdriver
from selenium.webdriver.common.by import By
import time

from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

from selenium.webdriver.chrome.options import Options
import subprocess

from selenium.webdriver.common.action_chains import ActionChains

subprocess.Popen(r'C:\Program Files\Google\Chrome\Application\chrome.exe --remote-debugging-port=9222 --user-data-dir="C:\chrometemp"') # 디버거 크롬 구동


class Test3:
    def __init__(self, address: str):
        option = Options()
        option.add_experimental_option("debuggerAddress", "127.0.0.1:9222")
        self.driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=option)
        self.driver.get(address)
        self.driver.maximize_window()

    # 로그인
    def LogIn(self):
        self.driver.get("")
        self.driver.maximize_window()
        self.driver.find_element(By.XPATH, "//*[@id='identifierId']").send_keys("dkgkejdrb")

    # 센터 리스트 > 잠실
    def center_list_check(self):
        self.driver.find_element(By.CLASS_NAME, "MuiSelect-select").click() # 센터 리스트
        self.driver.find_element(By.XPATH, "//li[@data-value='37']").click() # 잠실 센터

    # 로그아웃
    def LogOut(self):
        self.driver.find_element(By.XPATH, "//button[@tabindex='0']").click() # 로그아웃 버튼

    # 로그아웃 > 팝업창 > 승인
    def LogOut_Alert_Accept(self):
        from selenium.webdriver.common.alert import Alert
        da = Alert(self.driver)
        da.accept()

    def do(self):
        self.__init__("https://accounts.google.com/o/oauth2/auth/identifier?redirect_uri=storagerelay%3A%2F%2Fhttps%2Fzep.us%3Fid%3Dauth39217&response_type=permission%20id_token&scope=email%20profile%20openid&openid.realm&include_granted_scopes=true&client_id=193885560050-841lokv1235nmutetrie7p2mdof8k1kp.apps.googleusercontent.com&ss_domain=https%3A%2F%2Fzep.us&fetch_basic_profile=true&gsiwebsdk=2&flowName=GeneralOAuthFlow")
        self.LogIn()

if __name__ == '__main__':
    a = Test3("https://accounts.google.com/o/oauth2/auth/identifier?redirect_uri=storagerelay%3A%2F%2Fhttps%2Fzep.us%3Fid%3Dauth39217&response_type=permission%20id_token&scope=email%20profile%20openid&openid.realm&include_granted_scopes=true&client_id=193885560050-841lokv1235nmutetrie7p2mdof8k1kp.apps.googleusercontent.com&ss_domain=https%3A%2F%2Fzep.us&fetch_basic_profile=true&gsiwebsdk=2&flowName=GeneralOAuthFlow")
    a.do()
    print("clear")
